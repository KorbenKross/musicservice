package com.entities;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "Performer", schema = "public", catalog = "MusicService")
public class Performer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotBlank(message = "Имя не может быть пустым")
    @Length(max = 50, message = "Слишком длинное имя")
    private String name;

    @NotBlank(message = "Группа не может быть пустой")
    private String band;

    @NotBlank(message = "Страна не может быть пустой")
    private String country;

    private Integer listeners;

    public Performer() {
    }

    public Performer(String name, String band, String country) {
        this.name = name;
        this.band = band;
        this.country = country;
        this.listeners = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "band", nullable = false, length = 100)
    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    @Basic
    @Column(name = "country", nullable = false, length = 20)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "listeners", nullable = false)
    public Integer getListeners() {
        return listeners;
    }

    public void setListeners(Integer listeners) {
        this.listeners = listeners;
    }
}
