package com.controllers;

import com.entities.Album;
import com.entities.AuthorizedUser;
import com.entities.Song;
import com.services.AlbumService;
import com.services.SongService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class SongListController {
    private SongService songService;
    private AlbumService albumService;

    public SongListController(SongService songService, AlbumService albumService) {
        this.songService = songService;
        this.albumService = albumService;
    }

    @GetMapping("/songs")
    public String greeting(
            Map<String, Object> model) {
        Iterable<Song> songs = songService.findAllSongs();
        model.put("songs", songs);
        return "songs/songs";
    }

    @GetMapping("/songs/{song}")
    public String selectedPerformer(@PathVariable Song song, Model model) {
        model.addAttribute("song", song);
        return "songs/song";
    }

    @GetMapping("/addSong")
    public String addSong(Map<String, Object> model) {
        Iterable<Album> albums = albumService.findAllAlbums();
        model.put("albums", albums);
        return "songs/addSong";
    }

    @PostMapping("/songs/deleteSong/{song}")
    public String deleteSong(@PathVariable Song song, Model model) {
        songService.delete(song);
        return "redirect:/songs";
    }

    @PostMapping("/addSong")
    public String addSong(
            @AuthenticationPrincipal AuthorizedUser authorizedUser,
            @RequestParam String name,
            @RequestParam String albumName,
            @Valid Song song,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("song", song.getName());
            return "songs/addSong";
        } else {
            Song songFromDb = new Song(name);
            songService.addSong(songFromDb, albumName);
        }
        Iterable<Album> albums = albumService.findAllAlbums();
        model.addAttribute("albums", albums);
        Iterable<Song> songs = songService.findAllSongs();
        model.addAttribute("songs", songs);
        return "songs/songs";
    }

}
