package com.controllers;

import com.entities.AuthorizedUser;
import com.entities.Song;
import com.repos.SongRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class GreetingController {
    @Autowired
    SongRepo songRepo;

    @GetMapping("/")
    public String greeting(
            @RequestParam(name = "name", required = false, defaultValue = "World") String name,
            Map<String, Object> model) {
        model.put("name", name);
        return "greeting";
    }

    @GetMapping("/main")
    public String greeting(Map<String, Object> model) {
        return "main";
    }

    @PostMapping("/main")
    public String addSong(
            @AuthenticationPrincipal AuthorizedUser authorizedUser,
            @RequestParam String name,
            Map<String, Object> model) {
        Song song = new Song(name);
        songRepo.save(song);

        Iterable<Song> songs = songRepo.findAll();
        model.put("songs", songs);

        return "main";
    }
}
