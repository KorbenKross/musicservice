package com.controllers;

import com.entities.Album;
import com.entities.AuthorizedUser;
import com.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class Registration {

    private UserService userService;

    public Registration(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam String username,
            @RequestParam String password,
            @Valid AuthorizedUser authorizedUser,
            BindingResult bindingResult,
            Model model) {
        authorizedUser.setPassword(password);
        authorizedUser.setLogin(username);

        if (bindingResult.hasErrors()) {
            Map<String, String> errors = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errors);
            return "registration";
        }

        if (authorizedUser.getPassword() != null && !authorizedUser.getPassword().equals(authorizedUser.getPassword2())) {
            model.addAttribute("passwordError", "Пароли не совпадают");
            return "registration";
        } else {
            if (!userService.addUser(authorizedUser)) {
                model.addAttribute("loginError", "User exists!");
                return "registration";
            }
        }

        return "redirect:/login";
    }

}
