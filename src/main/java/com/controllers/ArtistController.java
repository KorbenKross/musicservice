package com.controllers;

import com.serviceAPI.RestClient;
import com.serviceAPI.jacksonparser.JacksonParser;
import com.serviceAPI.pojo.Artist;
import com.services.ArtistService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class ArtistController {
    private ArtistService artistService;
    Iterable<Artist> artists;

    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @PostMapping("/searchPerformers")
    public String searchPerformers(
            @RequestParam String name,
            Map<String, Object> model) {
        RestClient restClient = new RestClient();
        String resReq = restClient.get("artist.search&artist=" + name + "&api_key=e5e90c1dcef57aa97fd33bb2df47d105&format=json");
        JacksonParser jacksonParser = new JacksonParser(resReq);
        artists = jacksonParser.readValue().getResults().getArtistmatches().getArtist();
        model.put("artists", artists);
        return "main";
    }

    @GetMapping("/main/{artist}")
    public String selectedPerformer(@PathVariable("artist") String artist, Model model) {
        for (Artist artistFromList : artists) {
            if (artistFromList.getName().equals(artist)) {
                model.addAttribute("artist", artistFromList);
                model.addAttribute("artistImage", artistFromList.getImage().get(3).getText());
                artistService.addArtist(artistFromList);
                break;
            }
        }
        return "artists/artist";
    }

    @GetMapping("/artists")
    public String getArtistsList(
            Map<String, Object> model) {
        Iterable<com.entities.Artist> artists = artistService.findAllArtists();
        model.put("artists", artists);
        return "artists/artists";
    }
}
