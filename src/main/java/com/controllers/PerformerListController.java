package com.controllers;

import com.entities.AuthorizedUser;
import com.entities.Performer;
import com.entities.Song;
import com.repos.AlbumRepo;
import com.repos.PerformerRepo;
import com.services.AlbumService;
import com.services.PerformerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class PerformerListController {
    private AlbumService albumService;
    private PerformerService performerService;

    public PerformerListController(AlbumService albumService, PerformerService performerService) {
        this.albumService = albumService;
        this.performerService = performerService;
    }

    @GetMapping("/performers")
    public String performers(
            Map<String, Object> model) {
        Iterable<Performer> performers = performerService.findAllPerformers();
        model.put("performers", performers);
        return "performers/performers";
    }

    @GetMapping("/addPerformer")
    public String addPerformer(Map<String, Object> model) {
        return "performers/addPerformer";
    }

    @GetMapping("/performers/{performer}")
    public String selectedPerformer(@PathVariable Performer performer, Model model) {
        model.addAttribute("performer", performer);
        return "performers/performer";
    }

    @GetMapping("/performers/updatePerformer/{performer}")
    public String addPerformer(@PathVariable Performer performer, Map<String, Object> model) {
        model.put("performer", performer);
        return "performers/updatePerformer";
    }

    @PostMapping("/performers/deletePerformer/{performer}")
    public String deletePerformer(@PathVariable Performer performer, Model model) {
        performerService.delete(performer);
        return "redirect:/performers";
    }

    @PostMapping("/performers/updatePerformer/{performer}")
    public String updatePerformer(
            @PathVariable Performer performer,
            @RequestParam String name,
            @RequestParam String band,
            @RequestParam String country,
            Model model) {
        Performer performerUpdate = performerService.findByName(performer.getName());
        performerUpdate.setBand(band);
        performerUpdate.setCountry(country);
        performerUpdate.setName(name);
        performerService.savePerformer(performerUpdate);
        return "redirect:/performers";
    }

    @PostMapping("/addPerformer")
    public String addPerformer(
            @AuthenticationPrincipal AuthorizedUser authorizedUser,
            @RequestParam String name,
            @RequestParam String band,
            @RequestParam String country,
            @Valid Performer performer,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("performer", performer);
            return "performers/addPerformer";
        } else {
            Performer performerFromDb = new Performer(name, band, country);
            performerService.savePerformer(performerFromDb);
        }
        Iterable<Performer> performers = performerService.findAllPerformers();
        model.addAttribute("performers", performers);

        return "performers/performers";
    }

}
