package com.controllers;

import com.entities.Album;
import com.entities.AuthorizedUser;
import com.entities.Performer;
import com.services.AlbumService;
import com.services.PerformerService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class AlbumListController {
    private AlbumService albumService;
    private PerformerService performerService;

    public AlbumListController(AlbumService albumService, PerformerService performerService) {
        this.albumService = albumService;
        this.performerService = performerService;
    }

    @GetMapping("/albums")
    public String greeting(
            Map<String, Object> model) {
        Iterable<Album> albums = albumService.findAllAlbums();
        model.put("albums", albums);
        return "albums/albums";
    }

    @GetMapping("/addAlbum")
    public String addAlbum(Map<String, Object> model) {
        Iterable<Performer> performers = performerService.findAllPerformers();
        model.put("performers", performers);
        return "albums/addAlbum";
    }

    @GetMapping("/albums/{album}")
    public String selectedAlbum(@PathVariable Album album, Model model) {
        model.addAttribute("album", album);
        return "albums/album";
    }

    @PostMapping("/albums/deleteAlbum/{album}")
    public String deleteAlbum(@PathVariable Album album, Model model) {
        albumService.delete(album);
        return "redirect:/albums";
    }

    @PostMapping("/addAlbum")
    public String addSong(
            @AuthenticationPrincipal AuthorizedUser authorizedUser,
            @RequestParam String performer,
            @Valid Album album,
            BindingResult bindingResult,
            Model model) {
        Album albumFromDb = album;

        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("album", album.getName());
            Iterable<Performer> performers = performerService.findAllPerformers();
            model.addAttribute("performers", performers);
            return "albums/addAlbum";
        } else {
            albumService.addAlbum(albumFromDb, performer);
        }
        Iterable<Album> albums = albumService.findAllAlbums();
        model.addAttribute("albums", albums);
        return "albums/albums";
    }
}
