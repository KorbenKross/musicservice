package com.services;

import com.entities.Album;
import com.entities.Song;
import com.repos.AlbumRepo;
import com.repos.SongRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongService {
    private final SongRepo songRepo;
    private final AlbumRepo albumRepo;

    @Autowired
    public SongService(SongRepo songRepo, AlbumRepo albumRepo) {
        this.songRepo = songRepo;
        this.albumRepo = albumRepo;
    }

    public boolean addSong(Song song, String albumName) {
        Song songFromDb = songRepo.findByName(song.getName());
        if (songFromDb != null) {
            return false;
        }
        songFromDb = new Song();
        songFromDb.setName(song.getName());
        for (Album album : albumRepo.findAll()) {
            String albumFronDbName = album.getName().toLowerCase();
            if(albumFronDbName.equals(albumName.toLowerCase())){
                songFromDb.setAlbum(album);
            }
        }
        songRepo.save(songFromDb);
        return true;
    }

    public List<Song> findAllSongs() {
        return songRepo.findAll();
    }

    public void saveSong(Song song) {
        songRepo.save(song);
    }

    public void delete(Song song) {
        songRepo.delete(song);
    }

}
