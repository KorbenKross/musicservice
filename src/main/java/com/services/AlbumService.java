package com.services;

import com.entities.Album;
import com.entities.Performer;
import com.repos.AlbumRepo;
import com.repos.PerformerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumService {
    private final AlbumRepo albumRepo;
    private final PerformerRepo performerRepo;

    @Autowired
    public AlbumService(AlbumRepo albumRepo, PerformerRepo performerRepo) {
        this.performerRepo = performerRepo;
        this.albumRepo = albumRepo;
    }

    public boolean addAlbum(Album album, String performerName) {
        Album albumFromDb = albumRepo.findByName(album.getName());
        if (albumFromDb != null) {
            return false;
        }
        albumFromDb = new Album();
        albumFromDb.setName(album.getName());
        albumFromDb.setDate(album.getDate());
        albumFromDb.setStyle(album.getStyle());
        for (Performer performer : performerRepo.findAll()) {
            String performerFromDb = performer.getName().toLowerCase();
            if(performerFromDb.equals(performerName.toLowerCase())){
                albumFromDb.setAuthor(performer);
            }
        }
        albumRepo.save(albumFromDb);
        return true;
    }

    public List<Album> findAllAlbums() {
        return albumRepo.findAll();
    }

    public void saveAlbum(Album album) {
        albumRepo.save(album);
    }

    public void delete(Album album) {
        albumRepo.delete(album);
    }

}
