package com.services;

import com.entities.AuthorizedUser;
import com.entities.Role;
import com.repos.AuthorizedUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    private final AuthorizedUserRepo userRepo;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(AuthorizedUserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        return userRepo.findByLogin(login);
    }

    public boolean addUser(AuthorizedUser user) {
        AuthorizedUser userFromDb = userRepo.findByLogin(user.getLogin());
        if (userFromDb != null) {
            return false;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        userRepo.save(user);

        return true;
    }


    public List<AuthorizedUser> findAll() {
        return userRepo.findAll();
    }

    public void saveUser(AuthorizedUser user, String username, Map<String, String> form) {
        user.setLogin(username);

        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        userRepo.save(user);
    }
}
