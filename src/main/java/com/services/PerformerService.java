package com.services;

import com.entities.Performer;
import com.repos.AlbumRepo;
import com.repos.PerformerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerformerService {
    private final PerformerRepo performerRepo;
    private final AlbumRepo albumRepo;

    @Autowired
    public PerformerService(PerformerRepo performerRepo, AlbumRepo albumRepo) {
        this.performerRepo = performerRepo;
        this.albumRepo = albumRepo;
    }

    public Performer findByName(String performerName){
        return  performerRepo.findByName(performerName);
    }

    public boolean addPerformer(Performer performer) {
        Performer performerFromDb = performerRepo.findByName(performer.getName());
        if (performerFromDb != null) {
            return false;
        }
        performerFromDb = new Performer();
        performerFromDb.setName(performer.getName());
        performerFromDb.setListeners(Integer.valueOf(performer.getListeners()));
        performerFromDb.setCountry(performer.getCountry());
        performerFromDb.setCountry(performer.getBand());
        performerRepo.save(performerFromDb);
        return true;
    }

    public List<Performer> findAllPerformers() {
        return performerRepo.findAll();
    }

    public void savePerformer(Performer performer) {
        performerRepo.save(performer);
    }

    public void delete(Performer performer) {
        performerRepo.delete(performer);
    }

}
