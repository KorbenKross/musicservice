package com.services;

import com.entities.Artist;
import com.repos.ArtistRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtistService {
    private final ArtistRepo artistRepo;

    @Autowired
    public ArtistService(ArtistRepo artistRepo) {
        this.artistRepo = artistRepo;
    }

    public boolean addArtist(com.serviceAPI.pojo.Artist artist) {
        Artist artistFromDb = artistRepo.findByName(artist.getName());
        if (artistFromDb != null) {
            return false;
        }
        artistFromDb = new Artist();
        artistFromDb.setName(artist.getName());
        artistFromDb.setListeners(Integer.valueOf(artist.getListeners()));
        artistFromDb.setUrl(artist.getUrl());
        artistRepo.save(artistFromDb);
        return true;
    }

    public List<Artist> findAllArtists() {
        return artistRepo.findAll();
    }

    public void saveArtist(Artist artist) {
        artistRepo.save(artist);
    }

    public void delete(Artist artist) {
        artistRepo.delete(artist);
    }

}
