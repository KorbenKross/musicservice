package com.serviceAPI.jacksonparser;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serviceAPI.pojo.Example;

import java.io.IOException;

public class JacksonParser {

    private String jsonString;

    public JacksonParser(String jsonString) {
        this.jsonString = jsonString;
    }

    public Example readValue() {
        ObjectMapper mapper = new ObjectMapper();
        Example results = null;
        try {
            results = mapper.readValue(jsonString, Example.class);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }
}
