package com.repos;

import com.entities.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepo extends JpaRepository<Artist, Integer> {
    Artist findByName(String name);
}
