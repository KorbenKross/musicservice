package com.repos;

import com.entities.Performer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerformerRepo extends JpaRepository<Performer, Integer> {
    Performer findByName(String name);
}
