package com.repos;

import com.entities.Song;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepo extends JpaRepository<Song, Integer> {
    Song findByName(String name);
}
