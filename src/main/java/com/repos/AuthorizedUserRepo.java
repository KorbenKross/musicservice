package com.repos;

import com.entities.AuthorizedUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorizedUserRepo extends JpaRepository<AuthorizedUser, Integer> {
    AuthorizedUser findByLogin(String login);
}
