package com.repos;

import com.entities.Album;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepo extends JpaRepository<Album, Integer> {
    Album findByName(String name);
}
