<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium imp-medium">
    <div id="content">
        <article>
            <header class="major">
                <h2>${album.name}</h2>
                <span>${album.date}</span>
            </header>
            <p>${album.style}</p>
            <form class="auth_form_ftl" action="/albums/deleteAlbum/${album.id}" method="post">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <div class="auth_lbl">
                    <input type="submit" value="Удалить альбом"/>
                </div>
            </form>
        </article>
    </div>
</div>
</@c.page>