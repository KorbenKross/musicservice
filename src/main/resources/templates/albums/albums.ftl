<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium">
    <section class="box article-list">
        <h2 class="icon fa-file-text-o">Список альбомов</h2>
        <form class="auth_form_ftl" action="/addAlbum" method="get">
            <div class="auth_lbl">
                <input type="submit" value="Добавить альбом"/>
            </div>
        </form>
        <#list albums as album>
            <article class="box excerpt">
                <div>
                    <header>
                        <span class="date">${album.date}</span>
                        <h3><a href="/albums/${album.id}">${album.name}</a></h3>
                    </header>
                    <p>${album.style}</p>
                </div>
            </article>
        <#else>
            No albums
        </#list>
    </section>
</div>
</@c.page>