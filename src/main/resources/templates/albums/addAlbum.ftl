<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-12 col-12-medium">
    <section class="box article-list">
        <div class="auth_form_ftl"><h2 class="auth_form_gen">Добавить альбом</h2></div>
        <form class="auth_form_ftl addCommentForum" action="" method="post">
            <div><label class="auth_lbl"> Имя: <input class="${(nameError??)?string('is-valid', '')}"
                                                      value="<#if album??>${album}</#if>"
                                                      type="text" name="name"/> </label></div>
            <#if nameError??>
                <div style="color: #ff0000;" class="invalid-feedback">
                ${nameError}
                </div>
            </#if>
            <div><label class="auth_lbl"> Жанр: <input type="text" name="style"/> </label></div>
            <div><label class="auth_lbl"> Дата выпуска: <input id="currentDate" type="date" name="date"/> </label></div>
            <div>
                <label class="auth_lbl"> Исполнитель: <input id="performer" type="text" name="performer"/>
                    <select onchange="OnSelectionChange (this)">
                        <#list performers as performer>
                            <option>${performer.name}</option>
                        <#else>
                            Исполнителей нет
                        </#list>
                    </select>
                    <script  type="text/javascript">
                        document.getElementById('currentDate').valueAsDate = new Date();
                        function OnSelectionChange (select) {
                            var selectedOption = select.options[select.selectedIndex];
                            document.getElementById('performer').value = selectedOption.value.toUpperCase();
                        }
                    </script>
                </label>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="auth_lbl">
                <input type="submit" value="Добавить"/>
                <input type="reset" value="Очистить"/>
            </div>
        </form>
    </section>
</div>
</@c.page>