<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium imp-medium">
    <div id="content">
        <article>
            <header class="major">
                <h2>${song.name}</h2>
            </header>
            <form class="auth_form_ftl" action="/songs/deleteSong/${song.id}" method="post">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <div class="auth_lbl">
                    <input type="submit" value="Удалить композицию"/>
                </div>
            </form>
        </article>
    </div>
</div>
</@c.page>