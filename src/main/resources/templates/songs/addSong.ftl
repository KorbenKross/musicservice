<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-12 col-12-medium">
    <section class="box article-list">
        <div class="auth_form_ftl"><h2 class="auth_form_gen">Добавить композицию</h2></div>
        <form class="auth_form_ftl addCommentForum" action="" method="post">
            <div><label class="auth_lbl"> Название композиции: <input type="text"
                                                                      class="${(nameError??)?string('is-valid', '')}"
                                                                      value="<#if song??>${song.name}</#if>"
                                                                      name="name"/> </label></div>
            <#if nameError??>
                <div style="color: #ff0000;" class="invalid-feedback">
                ${nameError}
                </div>
            </#if>
            <div>
                <label class="auth_lbl"> Альбом: <input id="album" type="text" name="albumName"/>
                    <select onchange="OnSelectionChange (this)">
                        <#list albums as album>
                            <option>${album.name}</option>
                        <#else>
                            Альбомов нет
                        </#list>
                    </select>
                    <script  type="text/javascript">
                        function OnSelectionChange (select) {
                            var selectedOption = select.options[select.selectedIndex];
                            document.getElementById('album').value = selectedOption.value.toUpperCase();
                        }
                    </script>
                </label>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="auth_lbl">
                <input type="submit" value="Добавить"/>
                <input type="reset" value="Очистить"/>
            </div>
        </form>
    </section>
</div>
</@c.page>