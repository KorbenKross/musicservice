<#import "../parts/common.ftl" as c>
<@c.page>


<section class="box article-list">
    <form class="auth_form_ftl" action="/addSong" method="get">
        <div class="auth_lbl">
            <input type="submit" value="Добавить композицию"/>
        </div>
    </form>
    <h3 class="icon fa-file-text-o">Композиции</h3>
    <ul class="timeline">
        <#list songs as song>
            <li>
                <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <a href="/songs/${song.id}"><h4 class="timeline-title">${song.name}</h4></a>
                    </div>
                </div>
            </li>
        <#else>
            No songs
        </#list>
    </ul>
</section>
</@c.page>