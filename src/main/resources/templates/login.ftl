<#import "parts/common.ftl" as mainPage>
<#import "parts/login.ftl" as loginPage>
<@mainPage.page>
<div class="auth_form_ftl"><h1 class="auth_form_gen">Авторизация</h1></div>
    <@loginPage.login "/login" false/>
<a href="/registration">Добавить нового пользователя</a>
</@mainPage.page>