<#macro login path isRegisterForm>

<div class="col-12 col-12-medium">
    <section class="box article-list">
        <form id="authLogInForm" class="auth_form_ftl addCommentForum" action="${path}" method="post">
            <div><label class="auth_lbl"> Логин: <input class="${(loginError??)?string('is-valid', '')} logInInputForm"
                                                        value="<#if user??>${user.name}</#if>"
                                                        type="text" name="username"/> </label>
                <#if loginError??>
                    <div style="color: #ff0000;" class="invalid-feedback">
                    ${loginError}
                    </div>
                </#if>
            </div>
            <div><label class="auth_lbl"> Пароль: <input id="passwordInInputForm"
                                                         class="${(passwordError??)?string('is-valid', '')} passwordInInputForm"
                                                         type="password" name="password"/> </label>
                <#if passwordError??>
                    <div style="color: #ff0000;" class="invalid-feedback">
                    ${passwordError}
                    </div>
                </#if>
            </div>
            <#if isRegisterForm>
                <div><label class="auth_lbl"> Подтвердите пароль:
                    <input id="passwordInInputForm"
                           class="${(passwordError??)?string('is-valid', '')} passwordInInputForm"
                           type="password" name="password2"/> </label>
                    <#if passwordError??>
                        <div style="color: #ff0000;" class="invalid-feedback">
                        ${passwordError}
                        </div>
                    </#if>
                </div>
            </#if>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="auth_lbl"><input class="logInSubmitBtn" type="submit" value="Войти"/></div>
        </form>
    </section>
</div>
</#macro>

<#macro logout>
<div class="col-12 col-12-medium">
    <section class="box article-list">
        <form class="logoutForm" action="/logout" method="post">
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <input type="submit" value="Выход">
        </form>
    </section>
</div>
</#macro>