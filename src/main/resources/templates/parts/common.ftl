<#macro page>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Music Service</title>
    <link rel="stylesheet" href="/static/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
</head>
<body class="homepage is-preload">

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="/static/lightzoom/style.css" type="text/css">
<script type="text/javascript" src="/static/lightzoom/lightzoom.js"></script>

<div id="page-wrapper">
    <div id="header-wrapper">
        <div class="container">
            <header id="header">
                <div id class="blur_image"></div>
                <div class="inner">
                    <h1><a href="/main" id="logo">Music</a></h1>
                    <nav id="nav">
                        <ul>
                            <li class="current_page_item"><a href="/main">Главная</a></li>
                            <li><a href="/albums">Альбомы</a></li>
                            <li><a href="/performers">Исполнители</a></li>
                            <li><a href="/songs">Композиции</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
        </div>
    </div>
    <div id="main-wrapper">
        <div class="wrapper style3">
            <div class="inner">
                <div class="container">
                    <div class="row">
                        <#nested>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/authFormValidate.js"></script>
<script src="/static/js/registrationFormValidate.js"></script>
</body>
</html>
</#macro>