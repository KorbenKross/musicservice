<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet"/>
    <link rel="stylesheet" href="/static/greeting.css">
</head>
<body>
<div class="s01">
    <form action="/main">
        <fieldset>
            <legend>Music service / Last.fm</legend>
        </fieldset>
        <div class="input-field third-wrap">
            <button class="btn-search" type="submit">На главную</button>
        </div>
    </form>
</div>
</body>
</html>
