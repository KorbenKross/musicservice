<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium">
    <section class="box article-list">
        <h2 class="icon fa-file-text-o">Список сохраненных исполнителей last.fm</h2>
        <#list artists as artist>
            <article class="box excerpt">
                <div>
                    <header>
                        <h3><a href="/main/${artist.name}">${artist.name}</a></h3>
                    </header>
                    Слушателей: <p>${artist.listeners}</p>
                </div>
            </article>
        <#else>
            No artists
        </#list>
    </section>
</div>
</@c.page>