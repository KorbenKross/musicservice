<#import "../parts/common.ftl" as c>
<@c.page>
<div class="row">
    <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
        <div class="well profile">
            <div class="col-sm-12">
                <div class="col-xs-12 col-sm-8">
                    <h2>${artist.name}</h2>
                    <p><strong>URL: </strong> ${artist.url} </p>
                    <img src="${artistImage}" alt="user" class="img-circle img-responsive">
                </div>
            </div>
            <div class="col-xs-12 divider text-center">
                <div class="col-xs-12 col-sm-4 emphasis">
                    <h2><strong> ${artist.listeners} </strong></h2>
                    <p>
                        <small>Слушателей</small>
                    </p>
                    <a href="${artist.url}">Подписаться</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</@c.page>