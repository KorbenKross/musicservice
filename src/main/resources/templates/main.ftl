<#import "parts/common.ftl" as mainPage>
<#import "parts/login.ftl" as loginPage>

<@mainPage.page>
<div class="col-8 col-12-medium imp-medium">
    <div id="content">
        <article>
            <form class="search_toolbar" action="/searchPerformers" method="post">
                <div class="search_input">
                    <input type="text" name="name" placeholder="Исполнитель...">
                </div>
                <div class="search_input">
                    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                    <button type="submit">Найти</button>
                </div>
            </form>
            <form class="saved_artists" action="/artists">
                <p>Сохраненные исполнители last.fm</p>
                <div class="input-field third-wrap">
                    <button class="btn-search" type="submit">Исполнители</button>
                </div>
            </form>
            <div>Результаты поиска</div>
            <#if artists??>
                <#list artists as artist>
                    <div>
                        Имя: <a href="/main/${artist.name}"> <b>${artist.name}</b></a>
                        Слушателей: <span>${artist.listeners}</span>
                    </div>
                <#else>
                    Не найдено
                </#list>
            </#if>
            <div>
                <@loginPage.logout />
            </div>
        </article>
    </div>
</div>
</@mainPage.page>