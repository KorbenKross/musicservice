<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-12 col-12-medium">
    <section class="box article-list">
        <div class="auth_form_ftl"><h2 class="auth_form_gen">Добавить исполнителя</h2></div>
        <form class="auth_form_ftl addCommentForum" action="" method="post">
            <div><label class="auth_lbl"> Имя: <input type="text"
                                                      class="${(nameError??)?string('is-valid', '')}"
                                                      value="<#if performer??>${performer.name}</#if>"
                                                      name="name"/> </label></div>
            <#if nameError??>
                <div style="color: #ff0000;" class="invalid-feedback">
                ${nameError}
                </div>
            </#if>
            <div><label class="auth_lbl"> Бэнд(группа): <input type="text"
                                                               class="${(bandError??)?string('is-valid', '')}"
                                                               value="<#if performer??>${performer.band}</#if>"
                                                               name="band"/> </label></div>
            <#if bandError??>
                <div style="color: #ff0000;" class="invalid-feedback">
                ${bandError}
                </div>
            </#if>
            <div><label class="auth_lbl"> Страна: <input type="text" name="country"/> </label></div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="auth_lbl">
                <input type="submit" value="Добавить"/>
                <input type="reset" value="Очистить"/>
            </div>
        </form>
    </section>
</div>
</@c.page>