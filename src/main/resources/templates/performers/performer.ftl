<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium imp-medium">
    <div id="content">
        <!-- Content -->
        <article>
            <header class="major">
                <h2>${performer.name}</h2>
                <span>${performer.band}</span>
            </header>
            <span class="image featured"><img src="images/pic08.jpg" alt=""/></span>
            <p>${performer.country}</p>
            <form class="auth_form_ftl" action="/performers/deletePerformer/${performer.id}" method="post">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <div class="auth_lbl">
                    <input type="submit" value="Удалить"/>
                </div>
            </form>
            <form class="auth_form_ftl" action="/performers/updatePerformer/${performer.id}" method="get">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <div class="auth_lbl">
                    <input type="submit" value="Изменить"/>
                </div>
            </form>
        </article>

    </div>
</div>
</@c.page>