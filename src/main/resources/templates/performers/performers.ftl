<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-8 col-12-medium">
    <section class="box article-list">
        <form class="auth_form_ftl" action="/addPerformer" method="get">
            <div class="auth_lbl">
                <input type="submit" value="Добавить исполнителя"/>
            </div>
        </form>
        <h2 class="icon fa-file-text-o">Список исполнителей</h2>
        <#list performers as performer>
            <article class="box excerpt">
                <div>
                    <header>
                        <h3><a href="/performers/${performer.id}">${performer.name}</a></h3>
                    </header>
                    <p>${performer.band}</p>
                    <p>${performer.country}</p>
                </div>
            </article>
        <#else>
            No performers
        </#list>
    </section>
</div>
</@c.page>