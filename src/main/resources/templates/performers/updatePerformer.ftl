<#import "../parts/common.ftl" as c>
<@c.page>
<div class="col-12 col-12-medium">
    <section class="box article-list">
        <div class="auth_form_ftl"><h2 class="auth_form_gen">Изменить исполнителя</h2></div>
        <form class="auth_form_ftl addCommentForum" action="" method="post">
            <div><label class="auth_lbl"> Имя: <input type="text" name="name" value="${performer.name}"/> </label></div>
            <div><label class="auth_lbl"> Бэнд(группа): <input type="text" name="band" value="${performer.band}"/>
            </label></div>
            <div><label class="auth_lbl"> Страна: <input type="text" name="country" value="${performer.country}"/>
            </label></div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="auth_lbl">
                <input type="submit" value="Изменить"/>
                <input type="reset" value="Очистить"/>
            </div>
        </form>
    </section>
</div>
</@c.page>