var ulogin = document.querySelector('.registrationLoginInput');
var upassword = document.querySelector('.registrationPasswordInput');
var uname = document.querySelector('.registrationNameInput');
var umail = document.querySelector('.registrationEmailInput');
var usex = document.querySelector('.registrationSexInput');
var invalidLogin = document.getElementById('invalidLogin');
var invalidName = document.getElementById('invalidName');

document.getElementById('registrationLogInForm').addEventListener('submit', function (event) {
    event.preventDefault();
    checkMailValidation();
    checkUsername(ulogin, invalidLogin);
    checkPassword(3, 15);
    checkUsername(uname, invalidName);
    // checkSex(usex); Изменить элемент выбора пола на radio button
});

function checkUsername(uname, invalidUname) {

    if ($(uname).val() != '') {
        var pattern = /^[A-Za-z0-9]+$/;
        if (pattern.test($(uname).val())) {
            $(uname).css({'border': '1px solid #569b44'});
            console.log('Verno');
            $(invalidUname).text('Верно');
        } else {
            $(uname).css({'border': '1px solid #ff0000'});
            $(invalidUname).text('Не верно');
            console.log('Не верно');
        }
    } else {
        $(uname).css({'border': '1px solid #ff0000'});
        $(invalidUname).text('Поле не должно быть пустым');
        console.log('Поле не должно быть пустым');
    }
}

function checkPassword(minLength, maxLength) {
    if ($('#password').val() != '') {
        if (!($('#password').val().length >= maxLength) && !($('#password').val().length < minLength)) {
            $('#password').css({'border': '1px solid #569b44'});
            console.log('Verno');
            $('#invalidPassword').text('Верно');
        } else {
            $('#password').css({'border': '1px solid #ff0000'});
            $('#invalidPassword').text('Не верно');
            console.log('Не верно');
        }
    } else {
        $('#password').css({'border': '1px solid #ff0000'});
        $('#invalidPassword').text('Поле password не должно быть пустым');
        console.log('Поле password не должно быть пустым');
    }
}

function checkMailValidation() {
    if ($('#email').val() != '') {
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if (pattern.test($('#email').val())) {
            $('#email').css({'border': '1px solid #569b44'});
            console.log('Verno');
            $('#invalidMail').text('Верно');
        } else {
            $('#email').css({'border': '1px solid #ff0000'});
            $('#invalidMail').text('Не верно');
            console.log('Не верно');
        }
    } else {
        $('#email').css({'border': '1px solid #ff0000'});
        $('#invalidMail').text('Поле email не должно быть пустым');
        console.log('Поле email не должно быть пустым');
    }
}